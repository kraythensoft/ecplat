-- Created by Robert Simmons on 2/22/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved.
-- Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html

create table INVENTORY_ITEM (
  ID INT PRIMARY KEY,
  NAME varchar(255)
);
insert into INVENTORY_ITEM values (20531, 'GOEX');
insert into INVENTORY_ITEM values (20901, 'BCL');
