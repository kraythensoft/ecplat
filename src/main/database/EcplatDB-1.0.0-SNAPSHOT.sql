-- Created by Robert Simmons on 2/22/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved.
-- Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html

-- Eventually we will use Liquibase (http://www.liquibase.org/) to manage the database.

create table INVENTORY_ITEM (
  ID INT PRIMARY KEY,
  NAME varchar(255)
);

