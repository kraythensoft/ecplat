/* Created by Robert Simmons on 2/22/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved. */
/* Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html */
package com.kraythensoft.ecplat

import spray.json._

/** Model POSO for an inventory item. */
class InventoryItem(val id: Int, val name: String) {
  /** Default no-argument constructor; needed by  Squeryl. */
  def this() = this(0, "")

}

/** Companion object for inventory items. */
object InventoryItem {
  def apply(id: Int, name: String) = new InventoryItem(id, name)

  def unapply(item: InventoryItem) = {
    if (item == null) None
    else Some(item.id, item.name)
  }

  implicit val jsonFormat = new RootJsonFormat[InventoryItem] {
    def write(item: InventoryItem) = JsObject("id" -> JsNumber(item.id), "name" -> JsString(item.name))

    def read(value: JsValue) = {
      value.asJsObject.getFields("id", "name") match {
        case Seq(JsNumber(id), JsString(name)) => InventoryItem(id.toInt, name)
        case _ => throw new DeserializationException("Invalid JSON Value")
      }
    }
  }
}

/** spray-json format definitions for Ecplat model objects. */
object EcplatModelJsonFormat {
  implicit val inventoryItemJsonFormat = InventoryItem.jsonFormat
}

