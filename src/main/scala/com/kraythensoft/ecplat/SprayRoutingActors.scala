/* Created by Robert Simmons on 3/3/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved. */
/* Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html */
package com.kraythensoft.ecplat

import db.InventoryItemEPOActor
import spray.routing._
import akka.actor._
import spray.http._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import spray.json.DefaultJsonProtocol._
import spray.json._
import EcplatModelJsonFormat._
import HttpJsonFormats._
import spray.http.HttpResponse
import spray.can.server.HttpServer
import akka.routing.FromConfig


/** Base class for all spray-routing actors related to ecplat. */
abstract class EcplatRoutingActor extends Actor with HttpServiceActor with spray.util.SprayActorLogging {
  val jsonContentType = new ContentType(MediaTypes.`application/json`, Option(HttpCharsets.`UTF-8`))

  def jsonResponse[T: JsonFormat](data: T, status: StatusCode = StatusCodes.OK, headers: List[HttpHeader] = Nil, pretty: Boolean = false) = {
    val replyJSON = if (pretty) data.toJson.prettyPrint.stripMargin else data.toJson.compactPrint
    HttpResponse(status = status, entity = HttpBody(jsonContentType, replyJSON), headers = headers)
  }
}

/** The only job of this Actor is to route requests to other Actors and handle common issues such as timeouts. New in 0.4.0 is the integration of Spray-routing into
  * the class to simplify the routing of requests.
  * ''Note that we cant import {{spray.util._}} because of [[https://github.com/spray/spray/issues/207 Spray Issue #207]] therefore we have to use fully qualified
  * names for classes in that package. e.g. [[spray.util.SprayActorLogging]]''
  */
class RESTRoutingActor extends EcplatRoutingActor {
  implicit val timeout = Timeout(5 seconds)
  // Since we are using a router here there must be a configuration in the application.conf that defines akka.actor.deployment {/restRouting/inventoryRouter { ... } }
  // and the paths must match the deployed path below the /user node.
  val inventoryRoutingActor = context.actorOf(Props[InventoryRoutingActor].withRouter(new FromConfig()), "inventoryRouter")
  log.warning("inventoryRoutingActor @ " + inventoryRoutingActor.path)
  val debugRoutingActor = context.actorOf(Props[DebugRoutingActor], name = "debugRoutingActor")
  log.warning("debugRoutingActor @ " + debugRoutingActor.path)
  val adminRoutingActor = context.actorOf(Props[AdminRoutingActor], name = "adminRoutingActor")
  log.warning("adminRoutingActor @ " + adminRoutingActor.path)

  /** Converts the headers passed to a {{Map[String, String]}} of {{name -> value}} */
  def headersToStringMap(headers: List[spray.http.HttpHeader]) = Map[String, String](headers map {
    h => (h.name, h.value)
  }: _*)

  // Note that if you try to do any Akka code other than runRoute here, you are in for a hard time. Its best just to run the route only and put other code inside completions.
  def receive = runRoute {
    pathTest("debug") {
      ctx => context.actorFor(debugRoutingActor.path).forward(ctx)
    } ~ pathTest("admin") {
      ctx => context.actorFor(adminRoutingActor.path).forward(ctx)
    } ~ pathTest("inventory") {
      ctx => context.actorFor(inventoryRoutingActor.path).forward(ctx)
    } ~ complete {
      StatusCodes.NotFound
    }
  }
}

class InventoryRoutingActor extends EcplatRoutingActor {
  implicit val timeout = Timeout(5 seconds)

  def receive = runRoute {
    pathPrefix("inventory") {
      val dbActor = context.actorFor("/user/inventoryItemEPOActor")
      path("item" / IntNumber) {
        id =>
          complete {
            val future = (dbActor ? InventoryItemEPOActor.ItemByID(id)).mapTo[InventoryItem]
            future map (data => jsonResponse(data = data))
          }
      } ~ path("new") {
        parameters('id.as[Int], 'name.as[String]) {
          (id, name) => {
            dbActor ! InventoryItemEPOActor.NewItem(id, name)
            complete {
              val future = (dbActor ? InventoryItemEPOActor.ItemByID(id)).mapTo[InventoryItem]
              future map (data => jsonResponse(data = data))
            }
          }
        }
      } ~ path("items") {
        complete {
          val future = (dbActor ? InventoryItemEPOActor.AllItems).mapTo[List[InventoryItem]]
          future map (data => jsonResponse(data = data))
        }
      } ~ complete {
        StatusCodes.NotFound
      }
    }
  }
}

class DebugRoutingActor extends EcplatRoutingActor {
  /** Converts the headers passed to a {{Map[String, String]}} of {{name -> value}} */
  def headersToStringMap(headers: List[spray.http.HttpHeader]) = Map[String, String](headers map {
    h => (h.name, h.value)
  }: _*)

  // Note that if you try to do any Akka code other than runRoute here, you are in for a hard time. Its best just to run the route only and put other code inside completions.
  def receive = runRoute {
    get {
      pathPrefix("debug") {
        path("echoHeaders") {
          extract(_.request.headers) {
            headers => complete(jsonResponse(headersToStringMap(headers)))
          }
        }
      }
    } ~ complete {
      StatusCodes.NotFound
    }
  }
}

/** This actor provides routes relating to administering the server.. */
class AdminRoutingActor extends EcplatRoutingActor {
  implicit val timeout: Timeout = Duration(1, "sec")
  // for the actor 'asks' we use below

  def receive = runRoute {
    pathPrefix("admin") {
      path("serverStats") {
        complete {
          // This must match the name of the server instantiated in the boot up of Akka.
          val httpServerActor = context.actorFor("/user/spray-routing-http-server")
          val future = (httpServerActor ? HttpServer.GetStats).mapTo[HttpServer.Stats]
          future map (data => jsonResponse(data = data))
        }
      } ~ complete {
        StatusCodes.NotFound
      }
    }
  }
}