/* Created by Robert Simmons on 2/22/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved. */
/* Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html */
package com.kraythensoft.ecplat

import db.InventoryItemEPOActor
import spray.can.server.SprayCanHttpServerApp
import akka.actor.{Props, ActorSystem}
import org.apache.camel.component.file.FileComponent
import akka.camel.CamelExtension
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory

/** Application that runs the server. */
object Bootstrap extends App with SprayCanHttpServerApp {
  val config = ConfigFactory.load()
  // Start the Akka Bootstrap Actor System
  override lazy val system = ActorSystem("ecplat", config)

  // Start up the Apache Camel system and related actors
  val camel = CamelExtension(system)
  camel.context.addComponent("file", new FileComponent)
  val camelFileActor = system.actorOf(Props[CamelFileActor], name = "CamelFileActor")
  val activationFuture = camel.activationFutureFor(camelFileActor)(timeout = 10 seconds, executor = system.dispatcher)

  // Database actors (would at the end go into a router)
  val inventoryItemDBActor = system.actorOf(Props[InventoryItemEPOActor], name = "inventoryItemEPOActor")

  // Start up the spray-routing related HTTP Server and related actors
  val sprayRoutingServerActor = system.actorOf(Props[RESTRoutingActor], name = "restRouting")
  val httpServerRef = newHttpServer(handler = sprayRoutingServerActor, name = "spray-routing-http-server") ! Bind(interface = "localhost", port = 8090)
}
