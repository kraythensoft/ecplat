/* Created by Robert Simmons on 2/22/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved. */
/* Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html */
package com.kraythensoft.ecplat

import akka.camel.{CamelMessage, Consumer}
import akka.event.Logging

/** A general actor that will respond to simple text messages. */
class CamelFileActor extends Consumer {
  val log = Logging(context.system, this)

  /** TODO Configure this with some property, maybe mix in zookeeper */
  def endpointUri = "file:target/data/inbox"

  // old jetty uri "jetty:http://localhost:8877/ecplat"

  def receive = {
    case msg: CamelMessage => {
      log.info("Detected new file: ");
      val headers = msg.getHeaders
      log.info("Headers are: " + headers.toString)
      log.info("Body is: %s" format msg.bodyAs[String])
    }
  }
}

