/* Created by Robert Simmons on 2/22/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved. */
/* Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html */
package com.kraythensoft.ecplat.db

import akka.actor._

// [[org.squeryl.PrimitiveTypeMode]] imports are used to build a query dynamically.

import org.squeryl.PrimitiveTypeMode._
import com.kraythensoft.ecplat.db.InventoryItemEPOActor._
import com.kraythensoft.ecplat.InventoryItem

/** The base class for all Ecplat Persistence Operation actors. */
abstract class EcplatPersistenceOperationActor extends Actor {
}

/* Ecplat Persistence Operation Actors for [[InventoryItem]] and related messages. */
class InventoryItemEPOActor extends EcplatPersistenceOperationActor with spray.util.SprayActorLogging {
  // TODO Move the call to initDB elsewhere ... perhaps a connection pooling actor?
  initDB
  log.info("Using DB with url: " + dbloc)

  def receive = {
    case AllItems =>
      log.info("Processing %s".format(AllItems.getClass.getName))
      transaction {
        val results = from(schema.inventoryItems)(s => select(s)).toList
        log.info("Found %d results".format(results.size))
        log.info("Results are of type: %s".format(results.getClass.getName))
        sender ! results
      }
    case NewItem(id, name) =>
      log.info("Processing %s(%d, %s)".format(NewItem.getClass.getName, id, name))
      transaction {
        schema.inventoryItems.insert(new InventoryItem(id, name))
      }
    case ItemByID(id) =>
      log.info("Processing %s(%d)".format(ItemByID.getClass.getName, id))
      transaction {
        val results = from(schema.inventoryItems)(s => where(s.id === id) select (s)).toList
        sender ! results(0)
      }
    case msg =>
      log.error("Unhandled message of type %s sent to %s.".format(msg.getClass.getName, this.getClass.getName))
      log.error(msg.toString)
  }
}

/** Companion object to the actor which contains the case class and case object messages that the actor knows how to handle. Putting the messages in this object
  * gives them some isolation and reduces the chance of namespace collisions.
  */
object InventoryItemEPOActor {

  /** Base trait for messages to [[com.kraythensoft.ecplat.db.InventoryItemEPOActor]]. Sealer to prevent the user from adding more! */
  sealed trait EPOMsg

  /** Message to [[com.kraythensoft.ecplat.db.InventoryItemEPOActor]] that requests all [[com.kraythensoft.ecplat.InventoryItem]] objects. */
  case object AllItems extends EPOMsg;

  /** Message to [[com.kraythensoft.ecplat.db.InventoryItemEPOActor]] that requests the [[com.kraythensoft.ecplat.InventoryItem]] with the given id. */
  case class ItemByID(id: Int) extends EPOMsg;

  /** Message to [[com.kraythensoft.ecplat.db.InventoryItemEPOActor]] that adds an [[com.kraythensoft.ecplat.InventoryItem]] to the database. */
  case class NewItem(id: Int, name: String) extends EPOMsg;
}



