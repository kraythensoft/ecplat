/* Created by Robert Simmons on 2/22/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved. */
/* Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html */
package com.kraythensoft.ecplat.db

import org.squeryl.Schema
import com.kraythensoft.ecplat.InventoryItem

/** Schema for Squeryl access to model. */
class EcplatSchema extends Schema {
  val inventoryItems = table[InventoryItem]("INVENTORY_ITEM")
}
