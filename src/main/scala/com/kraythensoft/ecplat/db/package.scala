/* Created by Robert Simmons on 2/22/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved. */
/* Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html */
package com.kraythensoft.ecplat

import org.squeryl.{Session, SessionFactory}
import org.squeryl.adapters.H2Adapter
import sys.SystemProperties

package object db {
  // TODO The init of this should be moved to somewhere else. One idea is a connection pooling actor.
  /* Load the driver class to be used. */
  val driverClass = Class.forName("org.h2.Driver")
  /** Fetch the system property or connect to the in-memory DB */
  // TODO Need to put this in a config file or something.
  val dbloc = (new SystemProperties).getOrElse("ecplat.db.url", "")


  /** Initialize the database session factory */
  def initDB =
    if (SessionFactory.concreteFactory == None) {
      // Create a session factory to be used. This should be replaced by some kind of connection pooling
      SessionFactory.concreteFactory = Some(() => {
        Session.create(java.sql.DriverManager.getConnection(dbloc, "sa", ""), new H2Adapter)
      })
    }

  /** The schema instance used by the actors. */
  val schema = new EcplatSchema
}
