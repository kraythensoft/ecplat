/* Created by Robert Simmons on 3/8/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved. */
/* Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html */
package com.kraythensoft.ecplat

import concurrent.duration.FiniteDuration
import spray.can.server.HttpServer
import spray.json._

/** spray-json format objects for various HTTP objects, especially those used by spray-can. */
object HttpJsonFormats {
  implicit lazy val jsonFormatHttpServerStats = new RootJsonFormat[HttpServer.Stats] {
    def write(obj: HttpServer.Stats): JsValue = JsObject(
      "uptime" -> JsObject("length" -> JsNumber(obj.uptime.length), "unit" -> JsString(obj.uptime.unit.name)),
      "totalRequests" -> JsNumber(obj.totalRequests),
      "openRequests" -> JsNumber(obj.openRequests),
      "maxOpenRequests" -> JsNumber(obj.maxOpenRequests),
      "totalConnections" -> JsNumber(obj.totalConnections),
      "openConnections" -> JsNumber(obj.openConnections),
      "maxOpenConnections" -> JsNumber(obj.maxOpenConnections),
      "requestTimeouts" -> JsNumber(obj.requestTimeouts),
      "idleTimeouts" -> JsNumber(obj.idleTimeouts)
    )

    def read(json: JsValue): HttpServer.Stats = {
      val fields = json.asJsObject.fields
      val uptimeFields = fields.get("uptime").get.asJsObject.fields
      HttpServer.Stats(
        FiniteDuration(
          uptimeFields.get("length").get.asInstanceOf[JsNumber].value.toLong,
          uptimeFields.get("unit").get.asInstanceOf[JsString].value
        ),
        fields.get("totalRequests").get.asInstanceOf[JsNumber].value.toLong,
        fields.get("openRequests").get.asInstanceOf[JsNumber].value.toLong,
        fields.get("maxOpenRequests").get.asInstanceOf[JsNumber].value.toLong,
        fields.get("totalConnections").get.asInstanceOf[JsNumber].value.toLong,
        fields.get("openConnections").get.asInstanceOf[JsNumber].value.toLong,
        fields.get("maxOpenConnections").get.asInstanceOf[JsNumber].value.toLong,
        fields.get("requestTimeouts").get.asInstanceOf[JsNumber].value.toLong,
        fields.get("idleTimeouts").get.asInstanceOf[JsNumber].value.toLong
      )
    }
  }
}
