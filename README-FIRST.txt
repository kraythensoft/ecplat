/* Created by Robert Simmons on 2/22/13 - Copyright (c) 2013 KraythenSoft LLC. All rights reserved. */
/* Provided under the terms of the Apache Software License: http://www.apache.org/licenses/LICENSE-2.0.html */

Overview:
The Ecplat project is a demonstration of how to use Akka, Scala, Ajax and Gradle to build
a fully functional eCommerce platform. As the development of the example goes on the repository
will be tagged with important versions that add functionality to the system.

Prerequisites:
1) You will need to download and install Gradle 1.4 or greater.
2) The H2 Database system server must already be running before the server will work.

Command Examples:
The Ecplat system is designed primarily as a REST service and so it can be called with curl. Some commands that can
be used are below:
* Echo headers: curl -G "http://localhost:8090/debug/echoHeaders"
* Fetch all Inventory items: curl -G "http://localhost:8090/inventory/items"
* Fetch a single inventory item with the id of 11121: "http://localhost:8090/inventory/item/11121
* Add a new Inventory Item: curl -G "http://localhost:8090/inventory/new?id=11121&name=BC(L)2"
* Get the server stats: curl -G "http://localhost:8090/admin/serverStats"

Todo:
[ ] Add Authentication and authorization to the app. (single sign on tool?)
[ ] Upgrade to the new version of Akka & Spray with the new IO stack.
[ ] Investigate the need for spray-cache.
[ ] Implement other business objects such as customers and so on.
[ ] Implement a shopping cart and fully work out the inventory objects.
[ ] Investigate using Liquibase for Schema management in RDBMS situaiton.
[ ] Investigate adding support for using MongoDB or another NoSQL DB (this could be a branch)

Release Notes:
Version: 0.4.0    Git Tag: using-spray-routing
  * Reversed the README-FIRST.txt order to make it easier to read the most recent notes. However, new readers should
    read all old release notes first from the bottom up.
  * Replaced all the routing logic from the old spray-can HttpRequest pattern matching routing to spray-routing
    based routers.
  * Implemented an Akka router for handling inventory related calls. The router reads from the application.conf.
  * Changed the object Main t be Bootstrap to better reflect its purpose in bootstrapping Akka Microkernel
  * Refactored the JSON support and the method for returning a Json based response.
  * Added spray-json support for server stats.

Version: 0.3.0    Git Tag: added-spray-h2-and-squeryl
  * Created Skeleton framework for servicing REST requests.
  * Integrated Squeryl as the Persistence mechanism.
  * Implemented spray-can based actors for recieving messages.
  * Integrated H2 to make the backbone testing database.
    * Note that the user will have to look at the build.gradle file and alter the system property for ecplat.db.url
      and then run the TestData.sql file in src/main/resources to put in the schema and test objects. The default
      location is jdbc:h2:tcp://localhost/~/ecplat/ecplatdb
  * The user will now be able to use two commands, the easiest way is with curl
    * To view all items in the database:
      $ curl -H "ContentType: application/json" -G "http://localhost:8080/ecplat/inventory/allItems"
    * To add a new item with the ID '11121' and name 'BC(L)2' (note the quotes are important around the URL)
      $ curl -H "ContentType: application/json" -G "http://localhost:8080/ecplat/inventory/new?id=11121&name=BC(L)2"
  * The camel actor will still function
  * Implemented the use of basic spray-json abilities to serialize results to JSON
  * Broke up actors for handling REST replies into individual actors.
  * Johannes Rudolph solved an issue with the spray.util lib which resulted in a spray ticket and a workaround

Version: 0.2.1
  * Contributing Authors: Robert Simmons <kraythe@kraythensoft.com>
  * Added configuration for Log4j2 and SLF4J bindings.
  * Added inline documentation to build.gradle file.
  * Changed the Camel Use Case to read from a local directory instead of jetty so that I can integrate spray

Version: 0.2.0    Git Tag: with-camel-jetty
  * Contributing Authors: Robert Simmons <kraythe@kraythensoft.com>
  * Added new dependencies for Camel-jetty
  * Reworked starting Actors to resemble more the direction of the design
  * Changed the actors to use Camel
  * To run the microkernel server use "gradle run" or run the 'com.kraythensoft.store.Main' class in your IDE
  * You can send a message to the server with curl like this:
    curl --data "name=Robert" http://localhost:8877/ecplat

Version: 0.1.0    Git Tag: hello-ecplat
  * Contributing Authors: Robert Simmons <kraythe@kraythensoft.com>
  * Requires only Gradle 1.4 (everything else will be pulled by gradle)
  * Very basic getting started with Akka and Gradle
  * To use type at the command prompt: gradle build run
  * The system will send two messages and then exit
